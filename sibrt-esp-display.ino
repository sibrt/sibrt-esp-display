/*
 * Model: NodeMCU 0.9(ESP-12)
 * 
 */
/* 
 * Remote-Anzeige für SiBrT Brauthermometer
 * 
 * - Empfängt Daten via Websockets(Anzeige bei Verbindungsaufbau: "  :  "
 * - Zeigt abwechselnd Themperatur(7s) und Zeit(3s) an(sofern ein Timer läuft)
 * - Läutet die Glocke bei Alarm:
 *   - Themperatur: 4xkurz, Anzeige Temperatur
 *   - Zeit: 2x2 kurz, Anzeige "00:00"
 *   - Fehler: 4xlang, Anzeige ":  :  °"
 *
 */

#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <Adafruit_GFX.h>
#include "Adafruit_LEDBackpack.h"
#include <ArduinoJson.h>
#include <WebSocketsClient.h>
#include <SocketIOclient.h>

#define SIBRT_SERVER "192.168.1.1"
#define SIBRT_PORT 80

const char *ssid = "bierbier";
const char *password = NULL;

#define TIC_MS 312.5                    //312.5kHz
#define RCV_TIMEOUT 100*1000*TIC_MS    //10s
#define ALERT_PIN D7
#define MAX_ANZEIGE_ZEIT 6
#define MAX_ANZEIGE_THEMP 17
 

//Alarm Sequenz
#define ALARM_MAX 7
#define G 5
int gong[4]={G,250,G,250};
int fehler_alarm[ALARM_MAX]={G,1000,G,1000,G,1000,G};
int zeit_alarm[ALARM_MAX]={G,500,G,1000,G,500,G};
int themp_alarm[ALARM_MAX]={G,500,G,500,G,500,G};

SocketIOclient socket;

#define BUFFER_SIZE 1024
char buffer[BUFFER_SIZE];
StaticJsonDocument<JSON_OBJECT_SIZE(2)> update_data;
StaticJsonDocument<JSON_OBJECT_SIZE(2)> filter;


int temp_value=0;
int time_value=0;
int alarm_seq=-1;
int alarm_type=-1;
int display_ctr=0;
bool time_alarm=false;
bool temp_alarm=false;

#define DISPLAY_ADDRESS   0x70
#define SEG_NONE 0x10
#define SEG_CENTER 0x02
#define SEG_LEFTL 0x04
#define SEG_LEFTU 0x08
#define SEG_PONIT 0x10
#define SEG_DECORATION(a,b) a.writeDigitRaw(2,b)
#define USE_SERIAL Serial

Adafruit_7segment anzeige = Adafruit_7segment();

/** Timer Callback Funktion"*/
void ICACHE_RAM_ATTR onTimerISR(){
   //socket.disconnect();
   //ESP.restart();  //Reset on Error
}

void alarm(int seq[], int len)
{
  for(int i=0;i<len;i++)
  {
      digitalWrite(ALERT_PIN, !digitalRead(ALERT_PIN)); 
      delay(seq[i]);
  }
  digitalWrite(ALERT_PIN, LOW);  
}

void socketIOEvent(socketIOmessageType_t type, uint8_t * payload, size_t length) {
    switch(type) {
        case sIOtype_DISCONNECT:
            USE_SERIAL.printf("[IOc] Disconnected!\n");
            break;
        case sIOtype_CONNECT:
            USE_SERIAL.printf("[IOc] Connected to url: %s\n", payload);

            // join default namespace (no auto join in Socket.IO V3)
            socket.send(sIOtype_CONNECT, "/");
              
            break;
        case sIOtype_EVENT:
            USE_SERIAL.printf("[IOc] get event: %s\n", payload);
            if(length<10)
              break;
            if(strncmp("[\"temp1\",", (const char *)payload,9)==0)
            {
              update_temp((const char *)payload+9, length-10);
              break;
            }

            if(strncmp("[\"timer\",", (const char *)payload,9)==0)
            {
              update_time((const char *)payload+9, length-10);
              break;
            }
            break;
        case sIOtype_ACK:
            USE_SERIAL.printf("[IOc] get ack: %u\n", length);
            hexdump(payload, length);
            break;
        case sIOtype_ERROR:
            USE_SERIAL.printf("[IOc] get error: %u\n", length);
            hexdump(payload, length);
            break;
        case sIOtype_BINARY_EVENT:
            USE_SERIAL.printf("[IOc] get binary: %u\n", length);
            hexdump(payload, length);
            break;
        case sIOtype_BINARY_ACK:
            USE_SERIAL.printf("[IOc] get binary ack: %u\n", length);
            hexdump(payload, length);
            break;
    }
}

/**
 * - Setup Serial-Ouput, 115200 
 * - Setup Alert PIN direction
 * - Init Display 
 * - Setup Timer
 * - Setup Wifi
 * - Setup Multicast
 * 
 */
void setup() {
  delay(1000);
  
  Serial.begin(115200);
  Serial.println();
  
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(ALERT_PIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);
  digitalWrite(ALERT_PIN, LOW); 
  
  anzeige.begin(DISPLAY_ADDRESS);  
//  anzeige.setBrightness(2);
  SEG_DECORATION(anzeige,SEG_CENTER);
  anzeige.writeDisplay();
  
  alarm(gong,1);
  WiFi.begin(ssid,password);
  Serial.print("Connecting Wifi");
  bool blinken=false;
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
    blinken=(blinken==false);
    if(blinken==true)
       SEG_DECORATION(anzeige,SEG_CENTER); 
    else
       SEG_DECORATION(anzeige,SEG_PONIT); 
    anzeige.writeDisplay();
  }
  Serial.println();

  Serial.print("Connected, IP address: ");
  Serial.println(WiFi.localIP());
  Serial.print("Connect Wifi...OK");  

  filter["value"] = true;
  filter["status"] = true;
  socket.setExtraHeaders("Origin: http://192.168.1.1");
  socket.begin(SIBRT_SERVER, SIBRT_PORT, "/socket.io/?EIO=4");
  socket.onEvent(socketIOEvent);
  
  alarm(gong,4);
  Serial.print("Setup done, wait for data..");  
  SEG_DECORATION(anzeige,SEG_CENTER|SEG_LEFTU|SEG_LEFTL);
  anzeige.writeDisplay();
 
  timer1_attachInterrupt(onTimerISR);
  timer1_enable(TIM_DIV256, TIM_EDGE, TIM_SINGLE);
  timer1_write(RCV_TIMEOUT); 
  digitalWrite(LED_BUILTIN, HIGH);
}

bool parse_update(const char * payload, size_t length){

  if(length>=BUFFER_SIZE)
  {
    Serial.printf("Payload to big: %d\n", length);
    return false;
  }
  memcpy(buffer,payload,length);
  buffer[length]=0;
  DeserializationError err=deserializeJson(update_data, buffer, DeserializationOption::Filter(filter));
  if(err) {Serial.print(F("deserializeJson() failedx with code "));Serial.println(err.c_str());return false;}
  return true;
}
void update_temp(const char * payload, size_t length) {
  if(parse_update(payload,length)==false)
    return;
  temp_value=update_data["value"].as<double>()*1000;
  String status= update_data["status"].as<String>();
  if(status == "error")
    alarm_type=3;
  if(temp_value==0)
    alarm_type=1;
  Serial.printf("Temp: %d\n", temp_value);
  timer1_write(RCV_TIMEOUT);    //Restart Watchdog timer
}
void update_time(const char * payload, size_t length) {
  if(parse_update(payload,length)==false)
    return;
  time_value=update_data["value"];
  String status= update_data["status"].as<String>();
  if(status=="wait")
    time_value=-1;
  if(status=="off")
    time_value=-1;
  if(status=="ended")
    alarm_type=2;
  Serial.printf("Time: %d\n", time_value);
  timer1_write(RCV_TIMEOUT);    //Restart Watchdog timer
}
void loop() {
  for(int i=0;i<10;i++)
    socket.loop();
  
  // Display Ausgabe
  // ================
  if(alarm_type==0)
  {
    time_alarm=false;
    temp_alarm=false;
  }
  switch(alarm_type)
  {
    case 1:
      Serial.printf("Fehler Alarm\n");
      SEG_DECORATION(anzeige,SEG_CENTER|SEG_LEFTU|SEG_LEFTL);
      anzeige.writeDisplay();
      alarm(fehler_alarm,ALARM_MAX);
      break;
    case 2:
      if(time_alarm==true)
        break;
      Serial.printf("Teit Alarm\n");
      time_alarm=true;
      anzeige.writeDigitNum(0,0);
      anzeige.writeDigitNum(1,0);
      anzeige.writeDigitNum(2,0);
      anzeige.writeDigitNum(3,0);
      SEG_DECORATION(anzeige,SEG_CENTER);
      anzeige.writeDisplay();
      alarm(zeit_alarm,ALARM_MAX);
      break;
    case 3:
      if(temp_alarm==true)
        break;
      Serial.printf("Temperatur Alarm\n");
      temp_alarm=true;
      anzeige.print(temp_value/10,DEC);
      SEG_DECORATION(anzeige,0);
      anzeige.writeDisplay();
      alarm(themp_alarm,ALARM_MAX);
      break;
  }
  alarm_type=0;


  if(display_ctr<MAX_ANZEIGE_ZEIT && time_value>0)
  {
    int a=time_value%60;
    int m=time_value/60;
    a+=m*100;
    if(a>9959)
      a=9959;
    anzeige.print(a,DEC);
    if(a<10)
      anzeige.writeDigitNum(2,0);
    SEG_DECORATION(anzeige,SEG_CENTER);
  }
  else if(display_ctr<MAX_ANZEIGE_THEMP)
  {
    anzeige.print(temp_value/10,DEC);
    SEG_DECORATION(anzeige,0);
  }
  else
    display_ctr=0;
  
  display_ctr++;

  anzeige.writeDisplay();
  
  for(int i=0;i<10;i++)
  {
    socket.loop();
    delay(100);
  }
  }
